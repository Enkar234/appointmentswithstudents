package com.service.appointmentswithstudents.ui.login

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.services.cognitoidentityprovider.model.SignUpResult
import com.service.appointmentswithstudents.R
import com.service.appointmentswithstudents.activities.HomePageActivity
import com.service.appointmentswithstudents.data.CognitoHelper
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var cognitoHelper: CognitoHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        cognitoHelper = CognitoHelper(baseContext)

        val username = findViewById<EditText>(R.id.project_title)
        val password = findViewById<EditText>(R.id.subject)
        val login = findViewById<Button>(R.id.login)
        val register = findViewById<Button>(R.id.register)
        val loading = findViewById<ProgressBar>(R.id.loading)

        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory(filesDir))
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid
            register.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showOperationFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
                setResult(Activity.RESULT_OK)
                finish()
            }
        })

        loginViewModel.registerResult.observe(this@LoginActivity, Observer {
            val registerResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (registerResult.error != null) {
                showOperationFailed(registerResult.error)
            }
            if (registerResult.success != null) {
                val infoTextView = findViewById<TextView>(R.id.error_textview)
                infoTextView.text = "An email was sent to the given address. Click verification link to receive notifications."
                showOperationSucceded("User registered!")
            }
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            login.setOnClickListener {
                loading.visibility = View.VISIBLE
                val usernameString = username.text.toString()
                cognitoHelper.getUserPool().getUser(usernameString).getSessionInBackground(authenticationHandler)
            }

            register.setOnClickListener {
                loading.visibility = View.VISIBLE
                val usernameString = username.text.toString()
                val passwordString = password.text.toString()

                val userAttributes = CognitoUserAttributes()
                userAttributes.addAttribute("email", usernameString)
                cognitoHelper.getUserPool().signUpInBackground(usernameString, passwordString, userAttributes, null, signUpHandler)
            }
        }
    }

    private val signUpHandler: SignUpHandler = object : SignUpHandler {
        override fun onSuccess(user: CognitoUser, signUpResult: SignUpResult) {
            val username = findViewById<EditText>(R.id.project_title)
            val password = findViewById<EditText>(R.id.subject)
            loginViewModel.register(username.text.toString(), password.text.toString())
        }

        override fun onFailure(exception: Exception) {
            val errorTextView = findViewById<TextView>(R.id.error_textview)
            errorTextView.text = exception.message?.substringBefore("(")
            loading.visibility = View.GONE
        }
    }

    private val authenticationHandler : AuthenticationHandler = object : AuthenticationHandler {
        override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
            if (userSession == null || userSession.idToken == null || userSession.idToken.jwtToken == null) {
                val errorTextView = findViewById<TextView>(R.id.error_textview)
                errorTextView.text = "Couldn't obtain session token"
                loading.visibility = View.GONE
                return
            }
            val token = userSession.idToken.jwtToken
            val username = findViewById<EditText>(R.id.project_title)
            val password = findViewById<EditText>(R.id.subject)
            loginViewModel.login(username.text.toString(), password.text.toString(), token)
        }

        override fun onFailure(exception: Exception) {
            val errorTextView = findViewById<TextView>(R.id.error_textview)
            errorTextView.text = exception.message?.substringBefore("(")
            loading.visibility = View.GONE
        }

        override fun getAuthenticationDetails(authenticationContinuation: AuthenticationContinuation, userId: String) {
            val password = findViewById<EditText>(R.id.subject)
            val authenticationDetails = AuthenticationDetails(userId, password.text.toString(), null)
            authenticationContinuation.setAuthenticationDetails(authenticationDetails)
            authenticationContinuation.continueTask()
        }

        override fun authenticationChallenge(continuation: ChallengeContinuation?) {
            val a = 0
        }

        override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) {
            val a = 0
        }

    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName

        val mServiceIntent = Intent(this, HomePageActivity::class.java)
        if (!isMyServiceRunning(HomePageActivity::class.java)) {
            this.startActivity(mServiceIntent)
        }

        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("Service status", "Running")
                return true
            }
        }
        Log.i("Service status", "Not running")
        return false
    }

    private fun showOperationFailed(errorString: String) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    private fun showOperationSucceded(messageString: String) {
        Toast.makeText(applicationContext, messageString, Toast.LENGTH_SHORT).show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
