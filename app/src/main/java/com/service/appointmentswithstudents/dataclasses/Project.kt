package com.service.appointmentswithstudents.dataclasses

data class Project(val project_Id: String,
                   val title: String,
                   val subject: String,
                   val leader_Id: String,
                   var members: List<Member>,
                   var appointments: List<Appointment>? = null)