package com.service.appointmentswithstudents.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.service.appointmentswithstudents.dataclasses.Appointment
import com.service.appointmentswithstudents.R
import com.service.appointmentswithstudents.dataclasses.DeclaredAH
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HoursAdapter(private val context: Context,
                          private val dataSource: ArrayList<DeclaredAH>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): DeclaredAH {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.custom_list_item, parent, false)
        val left = rowView.findViewById(R.id.left_item) as TextView
        val right = rowView.findViewById(R.id.right_item) as TextView

        val declaredAH = dataSource[position]
        val dateFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val fromDate = dateFormat.format(Date(declaredAH.start))
        val toDate = dateFormat.format(Date(declaredAH.end))
        left.text = fromDate.substring(0, 10)
        right.text = "${fromDate.substring(11, 16)}-${toDate.toString().substring(11, 16)}"

        return rowView
    }
}