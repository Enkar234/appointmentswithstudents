package com.service.appointmentswithstudents.data

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.amazonaws.regions.Regions
import com.service.appointmentswithstudents.R

class CognitoHelper(context: Context) {
    private lateinit var userPool: CognitoUserPool

    private val userPoolId = context.resources.getString(R.string.user_pool_id)
    private val clientId = context.resources.getString(R.string.client_id)
    private val clientSecret = context.resources.getString(R.string.client_secret)
    private val cognitoRegion = Regions.US_EAST_1

    init {
        userPool = CognitoUserPool(context, userPoolId, clientId, clientSecret, cognitoRegion)
    }

    fun getUserPool() : CognitoUserPool {
        return userPool
    }
}