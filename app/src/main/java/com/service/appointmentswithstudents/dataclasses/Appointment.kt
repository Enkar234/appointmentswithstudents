package com.service.appointmentswithstudents.dataclasses

data class Appointment(
    val start: Long,
    val end: Long,
    val project_title: String,
    val appointment_Id: Int
)