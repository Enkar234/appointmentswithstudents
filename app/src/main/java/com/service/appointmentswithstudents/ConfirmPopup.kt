package com.service.appointmentswithstudents

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView

class ConfirmPopup(baseActivity : Activity, private val questionString : String, private val yesClicked: () -> Unit, private val noClicked: () -> Unit)
    : Dialog(baseActivity), View.OnClickListener {
    var yes: Button? = null
    var no: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.confirm_popup)

        val textView = findViewById<TextView>(R.id.middle_item)
        textView.text = questionString

        yes = findViewById(R.id.yes_button)
        no = findViewById(R.id.no_button)
        yes!!.setOnClickListener(this)
        no!!.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.yes_button -> yesClicked()
            R.id.no_button -> noClicked()
            else -> {
            }
        }
        dismiss()
    }
}