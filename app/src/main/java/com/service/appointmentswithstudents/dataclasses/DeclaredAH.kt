package com.service.appointmentswithstudents.dataclasses

data class DeclaredAH(
    val start: Long,
    val end: Long
)