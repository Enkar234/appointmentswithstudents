package com.service.appointmentswithstudents.activities

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.google.gson.Gson
import com.service.appointmentswithstudents.ConfirmPopup
import com.service.appointmentswithstudents.R
import com.service.appointmentswithstudents.data.UserConfig
import com.service.appointmentswithstudents.dataclasses.Member
import com.service.appointmentswithstudents.dataclasses.Project
import java.io.File

class CreateProjectActivity : AppCompatActivity() {
    private lateinit var configFilename : String
    private lateinit var userId : String
    private lateinit var adapter : ArrayAdapter<String>
    private val listItems = ArrayList<String>()
    private lateinit var token : String
    private lateinit var apiURL : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_project)

        configFilename = baseContext.resources.getString(R.string.config_filename)
        val userConfig = readConfig()
        if (userConfig == null) {
            Toast.makeText(this, "Couldn't read user id", Toast.LENGTH_LONG)
            return
        }
        userId = userConfig.userId
        apiURL = baseContext.resources.getString(R.string.api_url)

        val sessionTokenFilename = baseContext.resources.getString(R.string.token_filename)
        val file = File(filesDir, sessionTokenFilename)
        if(file.exists() && file.canRead()) {
            token = file.readText()
        }

        val listView = findViewById<ListView>(R.id.hours_list)

        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)

        listView.adapter = adapter
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                val memberToDelete = adapter.getItem(position)

                val yesFunction = {  confirmDeleteMemberClicked(memberToDelete)  }
                val noFunction = {  noClicked() }

                val popup = ConfirmPopup(
                    this,
                    "Remove member $memberToDelete from this list?",
                    yesFunction,
                    noFunction
                )
                popup.show()
            }

        adapter.notifyDataSetChanged()

    }

    private fun confirmDeleteMemberClicked(memberToDelete: String?) {
        if (memberToDelete == null) {
            return
        }
        listItems.removeAll { it == memberToDelete }
        adapter.notifyDataSetChanged()
    }

    private fun noClicked() {
        return
    }

    fun createProject(view: View) {
        val proj_title = findViewById<TextView>(R.id.project_title)
        val proj_subject = findViewById<TextView>(R.id.subject)

        if(proj_title.text.isEmpty()) {
            Toast.makeText(applicationContext, "Enter Project Title", Toast.LENGTH_LONG).show()
        }
        else if(proj_subject.text.isEmpty()) {
            Toast.makeText(applicationContext, "Enter Subject", Toast.LENGTH_LONG).show()
        }
        else {
            val members = mutableListOf<Member>()
            listItems.forEach {
                members.add(
                    Member(
                        it,
                        false
                    )
                )
            }
            members.add(
                Member(
                    userId,
                    true
                )
            )

            val project = Project(
                project_Id = "abc",
                title = proj_title.text.toString(),
                subject = proj_subject.text.toString(),
                leader_Id = userId,
                members = members
            )

            apiRequest(project)
        }
    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun addMember(view: View) {
        val member = findViewById<TextView>(R.id.member_email)
        val memberEmail = member.text.toString()
        if (isEmailValid(memberEmail)) {
            listItems.add(memberEmail)
            member.text = ""
            adapter.notifyDataSetChanged()
        }
        else {
            AlertDialog.Builder(this).setMessage("Invalid email!").show()
        }
    }

    private fun apiRequest(project : Project) {
        val gson = Gson()
        val members = gson.toJson(project.members)

        val progressBar = findViewById<ProgressBar>(R.id.create_progress_bar)
        progressBar.visibility = View.VISIBLE

        val url = apiURL + "create-project"
        Fuel.put(url, parameters = listOf("title" to project.title, "subject" to project.subject))
            .authentication()
            .bearer(token)
            .body(members)
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                if(response.statusCode !in 200..299){

                }
                if (bytes != null) {

                }
                if (response.statusCode == 200) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, "Project created", Toast.LENGTH_SHORT).show()
                        setResult(Activity.RESULT_OK)
                        this.finishAndRemoveTask()
                    }
                }
            }
    }
    private fun readConfig() : UserConfig? {
        val file = File(filesDir, configFilename)
        val gson = Gson()
        if(file.exists() && file.canRead()) {
            return gson.fromJson(file.readText(), UserConfig::class.java)
        }
        return null
    }
}