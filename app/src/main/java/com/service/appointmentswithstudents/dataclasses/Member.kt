package com.service.appointmentswithstudents.dataclasses

data class Member(
    val email: String,
    val is_leader: Boolean
)