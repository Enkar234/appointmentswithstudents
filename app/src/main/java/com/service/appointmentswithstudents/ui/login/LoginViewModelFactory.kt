package com.service.appointmentswithstudents.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.service.appointmentswithstudents.data.LoginDataSource
import com.service.appointmentswithstudents.data.LoginRepository
import java.io.File

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory(private val filesDir: File) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                loginRepository = LoginRepository(
                    dataSource = LoginDataSource(filesDir)
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
