package com.service.appointmentswithstudents.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.service.appointmentswithstudents.ConfirmPopup
import com.service.appointmentswithstudents.R
import com.service.appointmentswithstudents.adapters.HoursAdapter
import com.service.appointmentswithstudents.data.UserConfig
import com.service.appointmentswithstudents.dataclasses.Appointment
import com.service.appointmentswithstudents.dataclasses.DeclaredAH
import java.io.File
import java.text.SimpleDateFormat

class DeclareHoursActivity : AppCompatActivity() {
    private lateinit var adapter : HoursAdapter
    private val listItems = ArrayList<DeclaredAH>()
    private lateinit var availabilityHours : MutableList<DeclaredAH>
    private lateinit var userId : String
    private lateinit var configFilename : String
    private lateinit var apiURL : String
    private lateinit var sessionTokenFilename : String
    private lateinit var token : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.declare_hours)

        sessionTokenFilename = baseContext.resources.getString(R.string.token_filename)
        val file = File(filesDir, sessionTokenFilename)
        if(file.exists() && file.canRead()) {
            token = file.readText()
        }

        configFilename = baseContext.resources.getString(R.string.config_filename)
        apiURL = baseContext.resources.getString(R.string.api_url)

        val userConfig = readConfig()
        if (userConfig == null) {
            Toast.makeText(this, "Couldn't read user id", Toast.LENGTH_SHORT)
            setResult(Activity.RESULT_CANCELED)
            this.finishAndRemoveTask()
            return
        }
        userId = userConfig.userId

        val listView = findViewById<ListView>(R.id.hours_list)

        adapter = HoursAdapter(this, listItems)

        listView.adapter = adapter

        val declaredAHString = intent.getStringExtra("declaredAH") as String
        val gson = GsonBuilder().create()

        availabilityHours = gson.fromJson(declaredAHString, Array<DeclaredAH>::class.java).toMutableList()
        availabilityHours.forEach {
            listItems.add(it)
        }

        adapter.notifyDataSetChanged()

        listView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                val declarationToDelete = adapter.getItem(position)

                val yesFunction = {  confirmDeleteDeclarationClicked(declarationToDelete)  }
                val noFunction = {  noClicked() }

                val popup = ConfirmPopup(
                    this,
                    "Delete this declaration?",
                    yesFunction,
                    noFunction
                )
                popup.show()
            }
    }

    private fun confirmDeleteDeclarationClicked(declarationToDelete: DeclaredAH?) {
        if (declarationToDelete == null) {
            return
        }
        availabilityHours?.removeAll { it == declarationToDelete }
        listItems.remove(declarationToDelete)
        adapter.notifyDataSetChanged()
    }

    fun addAH(view: View) {
        val day = findViewById<TextView>(R.id.day_input)
        val fromHour = findViewById<TextView>(R.id.from_hour_input)
        val toHour = findViewById<TextView>(R.id.to_hour_input)

        if(day == null || day.text.isEmpty() || fromHour == null || fromHour.text.isEmpty() || toHour == null || toHour.text.isEmpty()) {
            Toast.makeText(this, "Fill all the fields first", Toast.LENGTH_SHORT).show()
            return
        }
        val dateFrom = SimpleDateFormat("dd-MM-yyyy HH:mm").parse("${day.text} ${fromHour.text}")
        if(dateFrom == null) {
            Toast.makeText(this, "Unable to parse date from", Toast.LENGTH_SHORT).show()
            return
        }
        val dateTo = SimpleDateFormat("dd-MM-yyyy HH:mm").parse("${day.text} ${toHour.text}")
        if(dateTo == null) {
            Toast.makeText(this, "Unable to parse date to", Toast.LENGTH_SHORT).show()
            return
        }
        if(dateFrom.time >= dateTo.time) {
            Toast.makeText(this, "Hour to must be grater than hour form", Toast.LENGTH_SHORT).show()
            return
        }

        val declared =
            DeclaredAH(
                dateFrom.time,
                dateTo.time
            )
        availabilityHours.add(declared)
        listItems.add(declared)
        adapter.notifyDataSetChanged()

        fromHour.text = ""
        toHour.text = ""
    }

    fun saveHours(view: View) {
        val yesFunction = {  confirmSaveClicked() }
        val noFunction = {  noClicked() }

        val popup = ConfirmPopup(
            this,
            "Save these declarations?",
            yesFunction,
            noFunction
        )
        popup.show()
    }

    private fun confirmSaveClicked() {
        apiRequest()
    }

    private fun readConfig() : UserConfig? {
        val file = File(filesDir, configFilename)
        val gson = Gson()
        if(file.exists() && file.canRead()) {
            return gson.fromJson(file.readText(), UserConfig::class.java)
        }
        return null
    }

    private fun apiRequest() {
        val url = apiURL + "declare-hours"
        val gson = Gson()
        val ah_string = gson.toJson(listItems)

        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("user_Id", userId))

        val progressBar = findViewById<ProgressBar>(R.id.declare_prog_bar)
        progressBar.visibility = View.VISIBLE

        Fuel.put(url, parameters = params)
            .authentication()
            .bearer(token)
            .body(ah_string)
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                if(response.statusCode !in 200..299){

                }
                if (response.statusCode == 200) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, "Declarations saved", Toast.LENGTH_SHORT).show()
                        setResult(Activity.RESULT_OK)
                        this.finishAndRemoveTask()
                    }
                }
            }
    }

    private fun noClicked() {
        return
    }

    override fun onDestroy() {
        super.onDestroy()
        setResult(Activity.RESULT_OK)
    }

}