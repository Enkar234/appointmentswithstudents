package com.service.appointmentswithstudents.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.service.appointmentswithstudents.dataclasses.Project
import com.service.appointmentswithstudents.R
import kotlin.collections.ArrayList

class ProjectAdapter(private val context: Context,
                          private val dataSource: ArrayList<Project>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Project {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.custom_list_item, parent, false)
        val left = rowView.findViewById(R.id.left_item) as TextView
        val right = rowView.findViewById(R.id.right_item) as TextView

        val project = dataSource[position]
        left.text = project.title
        right.text = project.subject

        return rowView
    }
}