package com.service.appointmentswithstudents.data

data class UserConfigs(
    var users: MutableMap<String, UserConfig>
)

data class UserConfig(
    val userId: String,
    val email: String,
    val passwordHash: String
)