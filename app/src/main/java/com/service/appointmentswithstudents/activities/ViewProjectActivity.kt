package com.service.appointmentswithstudents.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.service.appointmentswithstudents.ConfirmPopup
import com.service.appointmentswithstudents.R
import com.service.appointmentswithstudents.adapters.AppointmentsAdapter
import com.service.appointmentswithstudents.adapters.MembersAdapter
import com.service.appointmentswithstudents.data.UserConfig
import com.service.appointmentswithstudents.dataclasses.Appointment
import com.service.appointmentswithstudents.dataclasses.Member
import com.service.appointmentswithstudents.dataclasses.Project
import java.io.File

class ViewProjectActivity : AppCompatActivity() {
    private lateinit var membersAdapter : MembersAdapter
    private lateinit var appointmentsAdapter : AppointmentsAdapter
    private val membersListItems = ArrayList<Member>()
    private val appointmentsListItems = ArrayList<Appointment>()
    private lateinit var projectId : String
    private lateinit var userId : String
    private lateinit var configFilename : String
    private lateinit var sessionTokenFilename : String
    private lateinit var token : String
    private lateinit var apiURL : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_project)

        configFilename = baseContext.resources.getString(R.string.config_filename)
        sessionTokenFilename = baseContext.resources.getString(R.string.token_filename)

        val file = File(filesDir, sessionTokenFilename)
        if(file.exists() && file.canRead()) {
            token = file.readText()
        }
        apiURL = baseContext.resources.getString(R.string.api_url)

        val appointmentsListView = findViewById<ListView>(R.id.hours_list)
        val membersListView = findViewById<ListView>(R.id.list_of_members)

        appointmentsAdapter =
            AppointmentsAdapter(
                this,
                appointmentsListItems
            )
        membersAdapter =
            MembersAdapter(
                this,
                membersListItems
            )

        appointmentsListView.adapter = appointmentsAdapter
        membersListView.adapter = membersAdapter

        val projectString = intent.getStringExtra("project") as String

        val gson = GsonBuilder().create()
        val project = gson.fromJson(projectString, Project::class.java)

        projectId = project.project_Id

        project.members.forEach {
            membersListItems.add(it)
        }
        project.appointments?.forEach {
            appointmentsListItems.add(it)
        }

        notifyAdapters()

        val projectTitle = findViewById<TextView>(R.id.project_title)
        val subject = findViewById<TextView>(R.id.subject)
        projectTitle.text = project.title
        subject.text = project.subject

        val userConfig = readConfig()
        if (userConfig == null) {
            Toast.makeText(this, "Couldn't read user id", Toast.LENGTH_SHORT)
            setResult(Activity.RESULT_CANCELED)
            this.finishAndRemoveTask()
            return
        }
        userId = userConfig.userId
        if (userId != project.leader_Id) { //if i'm not the leader
            val projectLeaderStuff = mutableListOf(
                findViewById(R.id.add_members_label),
                findViewById(R.id.email_label),
                findViewById<TextView>(R.id.member_email),
                findViewById<Button>(R.id.add_member),
                findViewById<Button>(R.id.save_hours),
                findViewById<Button>(R.id.save_button),
                findViewById<Button>(R.id.plan_appontment_button)
            )

            projectLeaderStuff.forEach {
                it.visibility = View.GONE
            }
        }

        membersListView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                if (userConfig.userId != project.leader_Id) //if i'm not the leader
                    return@OnItemClickListener

                val memberToDelete = membersAdapter.getItem(position)

                if (memberToDelete.is_leader) //leader can't remove himself
                    return@OnItemClickListener

                val yesFunction = {  confirmDeleteMemberClicked(memberToDelete)  }
                val noFunction = {  noClicked() }

                val popup = ConfirmPopup(
                    this,
                    "Remove member ${memberToDelete.email} from this project?",
                    yesFunction,
                    noFunction
                )
                popup.show()
            }

        appointmentsListView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                if (userConfig.userId != project.leader_Id) //if i'm not the leader
                    return@OnItemClickListener

                val appointmentToDelete = appointmentsAdapter.getItem(position)

                val yesFunction = {  confirmCancelAppointmentClicked(appointmentToDelete)  }
                val noFunction = {  noClicked() }

                val popup = ConfirmPopup(
                    this,
                    "Cancel this appointment?",
                    yesFunction,
                    noFunction
                )
                popup.show()
            }
    }


    private fun confirmDeleteMemberClicked(memberToDelete: Member) {
        membersListItems.remove(memberToDelete)
        notifyAdapters()
    }

    private fun confirmCancelAppointmentClicked(appointmentToCancel: Appointment) {
        cancelAppointmentsApiRequest(appointmentToCancel)
    }

    private fun notifyAdapters(){
        membersAdapter.notifyDataSetChanged()
        appointmentsAdapter.notifyDataSetChanged()
    }

    fun addMember(view: View) {

        val yesFunction = {  confirmAddMemberClicked() }
        val noFunction = {  noClicked() }

        val popup = ConfirmPopup(
            this,
            "Are you sure you want to add this member?",
            yesFunction,
            noFunction
        )
        popup.show()
    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun confirmAddMemberClicked() {
        val memberEmail = findViewById<TextView>(R.id.member_email)
        val email = memberEmail.text.toString()
        if (isEmailValid(email)) {
            membersListItems.add(
                Member(
                    memberEmail.text.toString(),
                    false
                )
            )
            memberEmail.text = ""
            membersAdapter.notifyDataSetChanged()
            Toast.makeText(this, "Member added", Toast.LENGTH_SHORT).show()
        }
        else {
            AlertDialog.Builder(this).setMessage("Invalid email!").show()
        }
    }

    fun deleteProject(view: View) {

        val yesFunction = {  confirmLeaveOrRemoveClicked(true) }
        val noFunction = {  noClicked() }

        val popup = ConfirmPopup(
            this,
            "Are you sure you want to delete this project?",
            yesFunction,
            noFunction
        )
        popup.show()
    }

    fun leaveProject(view: View) {

        val yesFunction = {  confirmLeaveOrRemoveClicked(false) }
        val noFunction = {  noClicked() }

        val popup = ConfirmPopup(
            this,
            "Are you sure you want to leave this project?",
            yesFunction,
            noFunction
        )
        popup.show()
    }

    fun saveClicked(view: View) {

        val yesFunction = {  confirmSaveClicked() }
        val noFunction = {  noClicked() }

        val popup = ConfirmPopup(
            this,
            "Are you sure you want to save?",
            yesFunction,
            noFunction
        )
        popup.show()
    }

    private fun confirmSaveClicked() {
        editMembersApiRequest()
    }

    private fun confirmLeaveOrRemoveClicked(deleteProject: Boolean) {
        if(deleteProject) {
            deleteApiRequest()
        }
        else {
            leaveApiRequest()
        }
    }

    private fun noClicked() {
        return
    }

    fun planAppointment(view: View) {

        val yesFunction = {  planAppointmentClicked() }
        val noFunction = {  noClicked() }

        val popup = ConfirmPopup(
            this,
            "Plan appointment for this project?",
            yesFunction,
            noFunction
        )
        popup.show()
    }

    private fun planAppointmentClicked() {
        planAppointmentApiRequest()
    }

    private fun readConfig() : UserConfig? {
        val file = File(filesDir, configFilename)
        val gson = Gson()
        if(file.exists() && file.canRead()) {
            return gson.fromJson(file.readText(), UserConfig::class.java)
        }
        return null
    }

    private fun planAppointmentApiRequest() {
        val url = apiURL + "create-appointment"

        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("project_id", projectId))

        val progressBar = findViewById<ProgressBar>(R.id.proj_details_prog_bar)
        progressBar.visibility = View.VISIBLE

        Fuel.put(url, parameters = params)
            .authentication()
            .bearer(token)
            .body("")
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                if(response.statusCode !in 200..299){
                    runOnUiThread {
                        AlertDialog.Builder(this).setMessage(response.toString()).show()
                    }
                }
                if (response.statusCode == 200) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                    }
                }
                if (bytes != null) {
                    val gson = GsonBuilder().create()
                    val newAppointment = gson.fromJson(String(bytes), Appointment::class.java)
                    if (newAppointment == null || newAppointment.appointment_Id==-1) {
                        runOnUiThread {
                            AlertDialog.Builder(this).setMessage("Planning an appointment was not possible").show()
                        }
                    }
                    else {
                        appointmentsListItems.add(newAppointment)
                        runOnUiThread {
                            notifyAdapters()
                        }
                    }
                }
            }
    }

    private fun editMembersApiRequest() {
        val url = apiURL + "edit-project-members"
        val gson = Gson()
        val members_string = gson.toJson(membersListItems)

        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("project_id", projectId))

        val progressBar = findViewById<ProgressBar>(R.id.proj_details_prog_bar)
        progressBar.visibility = View.VISIBLE

        Fuel.put(url, parameters = params)
            .authentication()
            .bearer(token)
            .body(members_string)
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                if(response.statusCode !in 200..299){

                }
                if (response.statusCode == 200) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, "Changes saved", Toast.LENGTH_SHORT).show()
                        val saveProjectIntent = Intent()
                        val gson = Gson()
                        val membersString = gson.toJson(membersListItems)

                        saveProjectIntent.putExtra("members", membersString)
                        saveProjectIntent.putExtra("project_id", projectId)
                        setResult(Activity.RESULT_OK, saveProjectIntent)

                        this.finishAndRemoveTask()
                    }
                }
            }
    }

    private fun cancelAppointmentsApiRequest(appointmentToCancel: Appointment) {
        val url = apiURL + "delete-appointment"
        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("appointment_id", appointmentToCancel.appointment_Id.toString()))

        val progressBar = findViewById<ProgressBar>(R.id.proj_details_prog_bar)
        progressBar.visibility = View.VISIBLE

        Fuel.delete(url, parameters = params)
            .authentication()
            .bearer(token)
            .body("")
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                if(response.statusCode !in 200..299){

                }
                if (response.statusCode == 200) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, "Appointment deleted", Toast.LENGTH_SHORT).show()
                        appointmentsListItems.remove(appointmentToCancel)
                        notifyAdapters()
                    }
                }
            }
    }

    private fun leaveApiRequest() {
        val url = apiURL + "leave-project"

        val progressBar = findViewById<ProgressBar>(R.id.proj_details_prog_bar)
        progressBar.visibility = View.VISIBLE

        Fuel.delete(url, parameters = listOf("project_id" to projectId))
            .authentication()
            .bearer(token)
            .body("")
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                if(response.statusCode !in 200..299){

                }
                if (response.statusCode == 200) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, "You are no longer a member of this project", Toast.LENGTH_SHORT).show()
                        val removeProjectIntent = Intent()
                        removeProjectIntent.putExtra("project_to_remove", projectId)
                        setResult(Activity.RESULT_OK, removeProjectIntent)

                        this.finishAndRemoveTask()
                    }
                }
            }
    }

    private fun deleteApiRequest() {
        val url = apiURL + "delete-project"
        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("project_id", projectId))

        val progressBar = findViewById<ProgressBar>(R.id.proj_details_prog_bar)
        progressBar.visibility = View.VISIBLE

        Fuel.delete(url, parameters = params)
            .authentication()
            .bearer(token)
            .body("")
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                if(response.statusCode !in 200..299){

                }
                if (response.statusCode == 200) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, "Project deleted", Toast.LENGTH_SHORT).show()
                        val removeProjectIntent = Intent()
                        removeProjectIntent.putExtra("project_to_remove", projectId)
                        setResult(Activity.RESULT_OK, removeProjectIntent)

                        this.finishAndRemoveTask()
                    }
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        setResult(Activity.RESULT_OK)
    }
}