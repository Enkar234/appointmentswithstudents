package com.service.appointmentswithstudents.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.service.appointmentswithstudents.dataclasses.Member
import com.service.appointmentswithstudents.R
import kotlin.collections.ArrayList

class MembersAdapter(private val context: Context,
                          private val dataSource: ArrayList<Member>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Member {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.custom_list_item, parent, false)
        val left = rowView.findViewById(R.id.left_item) as TextView
        val right = rowView.findViewById(R.id.right_item) as TextView

        val member = dataSource[position]

        left.text = member.email
        if (member.is_leader) {
            right.text = "Leader"
        }

        return rowView
    }
}