package com.service.appointmentswithstudents.data

import com.google.gson.Gson
import org.mindrot.jbcrypt.BCrypt
import java.io.File
import java.lang.IllegalStateException
import java.util.*


/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource(private val filesDir: File) {

    private var configFilename = "config.json"
    private var allconfigsFilename = "allconfigs.json"
    private var tokenFilename = "sessionToken.txt"

    fun login(username: String, password: String, sessionToken: String): Result<UserConfig> {

        val userConfigs = readConfigs()

        return if (userConfigs != null && userConfigs.users.containsKey(username) && userConfigs.users[username] != null && BCrypt.checkpw(password, userConfigs.users[username]?.passwordHash)){
            saveToken(sessionToken)
            val config = userConfigs.users[username]!!
            saveConf(config, configFilename)
            Result.Success(config)
        } else {
            Result.Error(IllegalStateException("Unable to log in"))
        }
    }

    fun register(username: String, password: String): Result<UserConfig> {

        val userConfigs = readConfigs()
        return when {
            userConfigs == null -> {
                Result.Error(IllegalStateException("Couldn't read user config"))
            }
            userConfigs.users.containsKey(username) -> {
                Result.Error(IllegalStateException("User with this email already exists"))
            }
            else -> {
                val userConfig = UserConfig(
                    userId = username,
                    email = username,
                    passwordHash = BCrypt.hashpw(password, BCrypt.gensalt())
                )
                userConfigs.users[username] = userConfig
                saveConf(userConfigs, allconfigsFilename)
                saveConf(userConfig, configFilename)
                Result.Success(userConfig)
            }
        }
    }

    private fun saveToken(token : String) {
        val file = File(filesDir, tokenFilename)
        file.writeText(token)
    }

    private fun <T> saveConf(userConf: T, filename: String) {
        val gson = Gson()
        val jsonString = gson.toJson(userConf)

        val file = File(filesDir, filename)
        file.writeText(jsonString)
    }

    private fun readConfigs() : UserConfigs? {
        val file = File(filesDir, allconfigsFilename)
        val gson = Gson()
        return if (!file.exists()) {
            UserConfigs(mutableMapOf())
        } else if(file.canRead()) {
            gson.fromJson(file.readText(), UserConfigs::class.java)
        } else null
    }

    fun logout() {
        // TODO: revoke authentication
    }
}

