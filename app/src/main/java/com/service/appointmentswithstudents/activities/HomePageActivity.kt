package com.service.appointmentswithstudents.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.google.gson.Gson
import com.service.appointmentswithstudents.R
import com.service.appointmentswithstudents.data.CognitoHelper
import com.service.appointmentswithstudents.data.UserConfig
import com.service.appointmentswithstudents.dataclasses.Appointment
import com.service.appointmentswithstudents.dataclasses.Member
import com.service.appointmentswithstudents.dataclasses.Project
import java.io.File
import kotlin.concurrent.thread

class HomePageActivity : AppCompatActivity() {
    private lateinit var configFilename : String
    private lateinit var sessionTokenFilename : String
    private lateinit var token : String
    private lateinit var apiURL : String
    private lateinit var cognitoHelper: CognitoHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_page)

        configFilename = baseContext.resources.getString(R.string.config_filename)
        sessionTokenFilename = baseContext.resources.getString(R.string.token_filename)

        val file = File(filesDir, sessionTokenFilename)
        if(file.exists() && file.canRead()) {
            token = file.readText()
        }
        apiURL = baseContext.resources.getString(R.string.api_url)

        cognitoHelper = CognitoHelper(baseContext)
    }

    fun createProject(view: View) {
        val createProjectIntent = Intent(this, CreateProjectActivity::class.java)
        startActivity(createProjectIntent)
    }

    @SuppressLint("ShowToast")
    fun listProjects(view: View) {
        val userConfig = readConfig()
        if (userConfig == null) {
            Toast.makeText(this, "Couldn't read user id", Toast.LENGTH_LONG)
            return
        }
        val userId = userConfig.userId

        val url = apiURL + "get-projects"
        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("user_Id", userId))

        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)
        progressBar.visibility = View.VISIBLE

        thread(start = true) {
            for (i in 0..10) {
                val (_, response, result ) = Fuel.get(url, parameters = params)
                    .authentication()
                    .bearer(token)
                    .response()

                val (bytes, error) = result
                if (error != null) {

                }
                if(response.statusCode !in 200..299){

                }
                if (bytes != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                    }
                    val listProjectsIntent = Intent(this, ListProjectsActivity::class.java)
                    listProjectsIntent.putExtra("projects", String(bytes))
                    this.startActivity(listProjectsIntent)
                    return@thread
                }
            }
            runOnUiThread {
                progressBar.visibility = View.GONE
                AlertDialog.Builder(this)
                    .setMessage("Couldn't obtain list of projects\nTry later")
                    .show()
            }
        }
    }

    fun declareHours(view: View) {
        val userConfig = readConfig()
        if (userConfig == null) {
            Toast.makeText(this, "Couldn't read user id", Toast.LENGTH_LONG)
            return
        }
        val userId = userConfig.userId

        val url = apiURL + "get-hours"
        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("user_Id", userId))

        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)
        progressBar.visibility = View.VISIBLE

        thread(start = true) {
            for (i in 0..10) {
                val (_, response, result ) = Fuel.get(url, parameters = params)
                    .authentication()
                    .bearer(token)
                    .response()

                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        AlertDialog.Builder(this).setMessage(error.toString()).show()
                    }
                }
                if(response.statusCode !in 200..299){

                }
                if (bytes != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                    }
                    val declareHoursIntent = Intent(this, DeclareHoursActivity::class.java)
                    declareHoursIntent.putExtra("declaredAH", String(bytes))
                    this.startActivity(declareHoursIntent)
                    return@thread
                }
            }
            runOnUiThread {
                progressBar.visibility = View.GONE
                AlertDialog.Builder(this)
                    .setMessage("Couldn't obtain list of declared hours\nTry later")
                    .show()
            }
        }
    }

    fun viewAppointments(view: View) {
        val userConfig = readConfig()
        if (userConfig == null) {
            Toast.makeText(this, "Couldn't read user id", Toast.LENGTH_LONG)
            return
        }
        val userId = userConfig.userId

        val url = apiURL + "get-appointments"
        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("user_Id", userId))

        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)
        progressBar.visibility = View.VISIBLE

        thread(start = true) {
            for (i in 0..10) {
                val (_, response, result ) = Fuel.get(url, parameters = params)
                    .authentication()
                    .bearer(token)
                    .response()

                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        AlertDialog.Builder(this).setMessage(error.toString()).show()
                    }
                }
                if(response.statusCode !in 200..299){

                }
                if (bytes != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                    }
                    val listAppointmentsIntent = Intent(this, ListAppointmentsActivity::class.java)
                    listAppointmentsIntent.putExtra("appointments", String(bytes))
                    this.startActivity(listAppointmentsIntent)
                    return@thread
                }
            }
            runOnUiThread {
                progressBar.visibility = View.GONE
                AlertDialog.Builder(this)
                    .setMessage("Couldn't obtain list of appointments\nTry later")
                    .show()
            }
        }
    }

    fun logout(view: View) {
        Toast.makeText(
            applicationContext,
            "Logged out",
            Toast.LENGTH_LONG
        ).show()
        val configfile = File(filesDir, configFilename)
        configfile.delete()
        val tokenfile = File(filesDir, sessionTokenFilename)
        tokenfile.delete()
        finishAndRemoveTask()
    }
    private fun readConfig() : UserConfig? {
        val file = File(filesDir, configFilename)
        val gson = Gson()
        if(file.exists() && file.canRead()) {
            return gson.fromJson(file.readText(), UserConfig::class.java)
        }
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        val configfile = File(filesDir, configFilename)
        configfile.delete()
        val tokenfile = File(filesDir, sessionTokenFilename)
        tokenfile.delete()
    }
}