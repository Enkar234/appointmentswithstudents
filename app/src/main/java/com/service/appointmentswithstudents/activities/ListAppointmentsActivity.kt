package com.service.appointmentswithstudents.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.service.appointmentswithstudents.ConfirmPopup
import com.service.appointmentswithstudents.R
import com.service.appointmentswithstudents.adapters.AppointmentsAdapter
import com.service.appointmentswithstudents.adapters.ProjectAdapter
import com.service.appointmentswithstudents.dataclasses.Appointment
import com.service.appointmentswithstudents.dataclasses.Member
import com.service.appointmentswithstudents.dataclasses.Project
import java.io.File

class ListAppointmentsActivity : AppCompatActivity() {
    private lateinit var adapter : AppointmentsAdapter
    private val listItems = ArrayList<Appointment>()

    private lateinit var sessionTokenFilename : String
    private lateinit var token : String
    private lateinit var apiURL : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_appointments)

        sessionTokenFilename = baseContext.resources.getString(R.string.token_filename)
        val file = File(filesDir, sessionTokenFilename)
        if(file.exists() && file.canRead()) {
            token = file.readText()
        }
        apiURL = baseContext.resources.getString(R.string.api_url)

        val listView = findViewById<ListView>(R.id.appointments_list)

        adapter =
            AppointmentsAdapter(
                this,
                listItems
            )
        listView.adapter = adapter

        val appointmentsString = intent.getStringExtra("appointments") as String

        val gson = GsonBuilder().create()
        val appointments = gson.fromJson(appointmentsString, Array<Appointment>::class.java).toMutableList()
        appointments.sortBy { it.start }

        appointments.forEach {
            listItems.add(it)
        }

        adapter.notifyDataSetChanged()

        listView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                val selectedAppointment = adapter.getItem(position)

                val yesFunction = {  deleteAppointment(selectedAppointment)  }
                val noFunction = {  noClicked() }

                val popup = ConfirmPopup(
                    this,
                    "Cancel this appointment?",
                    yesFunction,
                    noFunction
                )
                popup.show()
            }
    }

    private fun deleteAppointment(appointment: Appointment) {
        val url = apiURL + "delete-appointment"

        val params : ArrayList<Pair<String, String>> = ArrayList(1)
        params.add(Pair("appointment_id", appointment.appointment_Id.toString()))

        val progressBar = findViewById<ProgressBar>(R.id.list_appointments_prog_bar)
        progressBar.visibility = View.VISIBLE

        Fuel.delete(url, parameters = params)
            .authentication()
            .bearer(token)
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                if(response.statusCode !in 200..299){
                    runOnUiThread {
                        AlertDialog.Builder(this).setMessage(response.toString()).show()
                    }
                }
                if (response.statusCode == 200) {
                    runOnUiThread {
                        progressBar.visibility = View.GONE
                        listItems.remove(appointment)
                        Toast.makeText(this, "Appointment canceled", Toast.LENGTH_SHORT).show()
                    }
                }
            }

    }

    private fun noClicked() {
        return
    }

    override fun onDestroy() {
        super.onDestroy()
        setResult(Activity.RESULT_OK)
        this.finishAndRemoveTask()
    }

}