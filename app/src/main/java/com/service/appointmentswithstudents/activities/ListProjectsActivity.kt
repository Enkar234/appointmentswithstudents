package com.service.appointmentswithstudents.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.service.appointmentswithstudents.R
import com.service.appointmentswithstudents.adapters.ProjectAdapter
import com.service.appointmentswithstudents.dataclasses.Member
import com.service.appointmentswithstudents.dataclasses.Project

class ListProjectsActivity : AppCompatActivity() {
    private val VIEW_PROJECT_ACTIVITY = 5
    private lateinit var adapter : ProjectAdapter
    private lateinit var projectString: String
    private lateinit var projects : MutableList<Project>
    private val listItems = ArrayList<Project>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_projects)

        projectString = intent.getStringExtra("projects") as String

        val listView = findViewById<ListView>(R.id.hours_list)

        adapter = ProjectAdapter(
            this,
            listItems
        )

        listView.adapter = adapter

        val gson = GsonBuilder().create()
        projects = gson.fromJson(projectString, Array<Project>::class.java).toMutableList()

        projects.forEach {
            listItems.add(it)
        }

        adapter.notifyDataSetChanged()

        listView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                val selectedProject = adapter.getItem(position)

                val gson = Gson()
                val selectedProjectString = gson.toJson(selectedProject)

                val selectProjectIntent = Intent(this, ViewProjectActivity::class.java)
                selectProjectIntent.putExtra("project", selectedProjectString)
                this.startActivityForResult(selectProjectIntent, VIEW_PROJECT_ACTIVITY)
            }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == VIEW_PROJECT_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                if(data != null) {
                    val projectIdToRemove = data.getStringExtra("project_to_remove")
                    if (projectIdToRemove != null) {
                        val projectToRemove = projects.first { it.project_Id == projectIdToRemove }
                        listItems.removeAll { it.project_Id == projectToRemove.project_Id }
                        projects.remove(projectToRemove)
                        adapter.notifyDataSetChanged()
                        return
                    }
                    val projectIdToChange = data.getStringExtra("project_id")
                    val membersString = data.getStringExtra("members")
                    if (projectIdToChange != null) {
                        val gson = GsonBuilder().create()
                        val members = gson.fromJson(membersString, ArrayList<Member>()::class.java)
                        var proj = projects.first { it.project_Id == projectIdToChange }
                        projects.removeAll { it.project_Id == projectIdToChange }
                        proj.members = members
                        projects.add(proj)
                    }
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        setResult(Activity.RESULT_OK)
        this.finishAndRemoveTask()
    }

}